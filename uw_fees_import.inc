<?php

/**
 * @file
 * Functions relating to importing fee data.
 */

/**
 *
 */
function uw_fees_import_page($filename) {
  $filename = drupal_get_path('module', 'uw_fees') . '/' . $filename;
  if (is_readable($filename)) {
    uw_fees_insert_tuition_data($filename);
    drupal_set_message(t('Import complete.'), 'status');
  }
  else {
    drupal_set_message(t('Import file not readable.'), 'error');
  }
  return 'Finished processing file.';
}

/**
 *
 */
function uw_fees_insert_tuition_data($filename) {
  // Read tuition set data from a CSV export of the IAP spreadsheet.
  if (($handle = fopen($filename, 'r')) !== FALSE) {
    // Mapping between columns (zero-based) and names of the data in those columns.
    $rate_id_cols = array(
      1 => 'program_term',
      2 => 'tuition_set_id',
    );
    $fields = array(
      11 => 'ca_tuition_max_0',
      12 => 'ca_tuition_max_1',
      13 => 'ca_tuition_max_2',

      15 => 'coop',
      16 => 'wrmf',

      19 => 'ca_tuition_per_course_0',
      20 => 'ca_tuition_per_course_1',
      21 => 'ca_tuition_per_course_2',
    );

    // Make an arrow of the first three column numbers. These are used for getting the terms.
    $term_keys = array();
    foreach (array_keys($fields) as $col) {
      $term_keys[] = $col;
      if (count($term_keys) === 3) {
        break;
      }
    }

    // Import all or nothing.
    $db = uw_fees_db();
    $txn = $db->startTransaction();

    $line = 0;
    $terms = FALSE;
    while (($data = fgetcsv($handle)) !== FALSE) {
      $line++;
      // Get the terms covered by this file. Only take the first three because the data in the fourth is non-final.
      if (!$terms && $data[$term_keys[0]] && $data[$term_keys[1]] && $data[$term_keys[2]]) {
        $terms = array(uw_fees_uw_term_convert_to_int($data[$term_keys[0]]), uw_fees_uw_term_convert_to_int($data[$term_keys[1]]), uw_fees_uw_term_convert_to_int($data[$term_keys[2]]));
      }
      // Insert a line of fee data.
      elseif ($terms && !empty($data[2])) {
        $tuition_set = array();
        foreach ($rate_id_cols as $col => $field) {
          $tuition_set[$field] = $data[$col];
        }
        foreach ($fields as $col => $field) {
          // Convert amounts into integer cents. preg_replace() gets rid of comma thousands separaters.
          $tuition_set[$field] = (int) (preg_replace('/[^0-9.]+/', '', $data[$col]) * 100);
          // Store NULL instead of zero.
          if (!$tuition_set[$field]) {
            $tuition_set[$field] = NULL;
          }
        }

        // Diploma programs with 1 term.
        if (!preg_match('/^[1-5][AB]?$/', $tuition_set['program_term'])) {
          $tuition_set['program_term'] = 00;
        }

        // Insert entries, one for each term. $term_key is 0, 1, or 2.
        foreach ($terms as $term_key => $term_id) {
          $insert = array(
            'tuition_set_id' => $tuition_set['tuition_set_id'],
            'term_id' => $term_id,
            'program_term' => $tuition_set['program_term'],
            'ca_tuition_max' => $tuition_set['ca_tuition_max_' . $term_key],
            'ca_tuition_per_course' => $tuition_set['ca_tuition_per_course_' . $term_key],
            'coop' => $tuition_set['coop'],
            'wrmf' => $tuition_set['wrmf'],
          );
          try {
            $db->insert('tuition_set_term')->fields($insert)->execute();
          }
          catch (Exception $e) {
            $txn->rollback();
            throw $e;
          }
        }
      }
    }
    fclose($handle);
  }
}

/**
 *
 */
function uw_fees_insert_tuition_data_grad($filename) {
  // Read tuition set data from a CSV export of the IAP spreadsheet.
  $all_data = array();
  if (($handle = fopen($filename, 'r')) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, "\t")) !== FALSE) {
      $data = array_combine(array('code', 'program_title', 1, 2, 'intca', 3, 4, 'ftpt', 'amount', 'termcourse'), $data);
      unset($data[1]);
      unset($data[2]);
      unset($data[3]);
      unset($data[4]);

      if ($data['code']) {
        $code = $data['code'];
        $program_title = $data['program_title'];
        $intca = 'ca';
        $all_data[$code] = array('ca_tuition_max' => NULL, 'int_tuition_max' => NULL, 'pt_ca_tuition' => NULL, 'pt_int_tuition' => NULL);
      }
      if (strpos($data['intca'], 'International') !== FALSE) {
        $intca = 'int';
      }

      if (strpos($data['termcourse'], 'term') !== FALSE) {
        $pt_per_course = FALSE;
      }
      else {
        $pt_per_course = TRUE;
      }

      if (strpos($data['ftpt'], 'Full') !== FALSE) {
        $ft = TRUE;
      }
      elseif (strpos($data['ftpt'], 'Part') !== FALSE) {
        $ft = FALSE;
      }
      else {
        $ft = !$pt_per_course;
      }

      if ($ft) {
        $key = $intca . '_tuition_max';
      }
      else {
        $key = 'pt_' . $intca . '_tuition';
      }

      $all_data[$code][$key] = ((int) preg_replace('/[^0-9]/', '', $data['amount']));
      $all_data[$code]['pt_per_course'] = $pt_per_course;
      $all_data[$code]['program_title'] = $program_title;
    }
    fclose($handle);
  }

  // Import all or nothing.
  $db = uw_fees_db();
  $txn = $db->startTransaction();
  foreach ($all_data as $tuition_set_id => $data) {
    $a = $data += array(
      'tuition_set_id' => $tuition_set_id,
      'term_id' => 1135,
      'program_term' => '',
    );
    unset($a['program_title']);

    $a['pt_per_course'] = (int) $a['pt_per_course'];
    dsm($a);
    try {
      $db->insert('tuition_set_term')->fields($a)->execute();
    }
    catch (Exception $e) {
      $txn->rollback();
      throw $e;
    }

    $a = array(
      'group_code' => 'GRAD',
      'program_title' => $data['program_title'],
      'tuition_set_id' => $tuition_set_id,
    );
    dsm($a);
    try {
      $db->insert('program')->fields($a)->execute();
    }
    catch (Exception $e) {
      $txn->rollback();
      throw $e;
    }
  }
}
