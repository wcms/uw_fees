<?php

/**
 * @file
 * Functions relating to displaying the program tables.
 */

/**
 *
 */
function uw_fees_display_programs() {
  $output = array();

  // Query all groups.
  $q = 'SELECT DISTINCT group_code, COALESCE(group_full_name, group_short_name) AS group_name
    FROM {program} JOIN {uw_group} USING(group_code)
    WHERE group_code != \'GRAD\'
    ORDER BY group_name';
  $groups = uw_fees_db()->query($q);

  // Prepare query of all programs in a group.
  $q = 'SELECT program_title, {program}.fee_set_id, tuition_set_id, int_tuition_set_id, program_term, {program_program_term}.fee_set_id AS fee_set_id_exception
    FROM {program}
    LEFT JOIN program_program_term USING(program_title)
    WHERE group_code = :group_code
    ORDER BY
    program_title LIKE \'Co-op%\', program_title LIKE \'Regular%\',
    program_title';
  $prep = uw_fees_db()->prepareQuery($q);

  // Foreach group, make a table of the programs in that group.
  while ($group = $groups->fetch()) {
    $prep->execute(array('group_code' => $group->group_code));

    $programs = array();
    while ($program = $prep->fetch()) {
      if (empty($programs[$program->program_title])) {
        $programs[$program->program_title] = array(
          'fee_set_id' => $program->fee_set_id,
          'tuition_set_id' => $program->tuition_set_id,
          'fee_set_id_exceptions' => array(),
        );
      }
      // Gather fee_set_id_exceptions from program_program_term. Most programs have only NULLs in program_program_term.
      if ($program->program_term) {
        $programs[$program->program_title]['fee_set_id_exceptions'][$program->program_term] = $program->fee_set_id_exception;
      }
    }

    $rows = array();
    foreach ($programs as $program_title => $program) {
      $fee_set_id = $program['fee_set_id'];
      // Add list of fee_set_id exceptions, if any.
      if ($program['fee_set_id_exceptions']) {
        $items = array();
        foreach ($program['fee_set_id_exceptions'] as $year => $exception) {
          $items[] = $year . ': ' . $exception;
        }
        $fee_set_id .= '<p>Exception terms:</p>' . theme('item_list', array('items' => $items));
      }
      $rows[] = array(
        'data' => array(
          $program_title,
          $fee_set_id,
          $program['tuition_set_id'],
        ),
        'no_striping' => TRUE,
      );
    }

    $output[$group->group_code]['header']['#markup'] = '<h2>' . $group->group_name . ' (' . $group->group_code . ')</h2>';
    $output[$group->group_code]['content'] = array(
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => array('Program title', 'fee_set_id', 'tuition_set_id'),
    );
  }

  return $output;
}
