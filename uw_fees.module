<?php

/**
 * @file
 * Main hook implementations and shared functions.
 */

/**
 * Implements hook_menu().
 */
function uw_fees_menu() {
  $items = array();
  $items['student-accounts/tuition-fee-schedules'] = array(
    'title' => 'Tuition fee schedules',
    'page callback' => 'uw_fees_display_term_list',
    'page arguments' => array(),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  $items['student-accounts/tuition-fee-schedules/programs'] = array(
    'title' => 'Programs',
    'page callback' => 'uw_fees_display_programs',
    'page arguments' => array(),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'uw_fees_programs.inc',
  );

  $items['student-accounts/tuition-fee-schedules/import/%'] = array(
    'title' => 'Import tution data',
    'page callback' => 'uw_fees_import_page',
    'page arguments' => array(3),
    'access arguments' => array('administer nodes'),
    'type' => MENU_CALLBACK,
    'file' => 'uw_fees_import.inc',
  );

  $items['student-accounts/tuition-fee-schedules/incidental-fees'] = array(
    'title' => 'Incidental fees',
    'page callback' => 'uw_fees_display_incidental',
    'page arguments' => array(),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'uw_fees_incidental.inc',
  );

  $items['student-accounts/tuition-fee-schedules/%'] = array(
    'title' => 'Tuition fee schedules',
    'page callback' => 'uw_fees_display_table',
    'page arguments' => array(2),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'uw_fees_tuition.inc',
  );
  $items['student-accounts/tuition-fee-schedules/%/%'] = array(
    'title' => 'Tuition fee schedules',
    'page callback' => 'uw_fees_display_table',
    'page arguments' => array(2, 3),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'uw_fees_tuition.inc',
  );
  $items['student-accounts/tuition-fee-schedules/%/%/%'] = array(
    'title' => 'Tuition fee schedules',
    'page callback' => 'uw_fees_display_table',
    'page arguments' => array(2, 3, 4),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'uw_fees_tuition.inc',
  );

  $items['student-accounts/tuition-fee-schedules/%/%/%/%'] = array(
    'page callback' => 'drupal_not_found',
    'access callback' => TRUE,
  );
  return $items;
}

/**
 * Implements hook_theme().
 */
function uw_fees_theme() {
  return array(
    'uw_fees_fee_table' => array(
      'variables' => array('full_program' => NULL, 'show_tuition_5th_course' => NULL, 'career' => NULL),
    ),
  );
}

/**
 *
 */
function uw_fees_uw_term_convert_to_int($term) {
  if (preg_match('/([FWS])([0-9]+)/', $term, $BR)) {
    $month = $BR[1];
    $year = (int) $BR[2];

    // Convert year into calendar_year - 1900.
    // 4 digit year.
    if ($year >= 1900) {
      $year = $year - 1900;
    }
    // 2 digit year for 2000 and after. Other 2 digit years do not require changes.
    elseif ($year < 57) {
      $year += 100;
    }
    // Invalid year.
    elseif ($year >= 100) {
      return NULL;
    }

    $mapping = array('F' => 9, 'W' => 1, 'S' => 5);
    $month = $mapping[$month];

    return (int) ($year . $month);
  }
  else {
    return NULL;
  }
}

/**
 *
 */
function uw_fees_display_term_list() {
  $q = 'SELECT DISTINCT term_id FROM {tuition_set_term} ORDER BY term_id DESC';
  $r = uw_fees_db()->query($q);
  $items = array();
  while ($term = $r->fetch()) {
    $items[] = l(uw_value_lists_uw_term_number_to_string($term->term_id), 'student-accounts/tuition-fee-schedules/' . $term->term_id);
  }
  return array(
    '#theme' => 'item_list',
    '#items' => $items,
    '#type' => 'ul',
  );
}

/**
 * Formats an amount of money.
 *
 * @param int $amount
 *   The amount of money in cents.
 *
 * @return string
 *   The formatted amount.
 */
function uw_fees_format_money($amount) {
  if (is_numeric($amount)) {
    return '$' . number_format($amount / 100, 2);
  }
}

/**
 * @return DatabaseConnection
 *   The database connection object for the fees database.
 */
function uw_fees_db() {
  return Database::getConnection('default', 'sitestatus');
}
