<?php

/**
 * @file
 * Functions relating to displaying complete tuition tables for student use.
 */

/**
 *
 */
function uw_fees_display_table($term_id, $int_ca = 'ca', $career = 'ugrad') {
  $term_id = (int) $term_id;
  if (!$term_id || !in_array($int_ca, array('int', 'ca'), TRUE) || !in_array($career, array('ugrad', 'grad'), TRUE)) {
    return drupal_not_found();
  }

  $title = 'Tuition fee schedules ' . uw_value_lists_uw_term_number_to_string($term_id);
  $title .= ', ' . (($int_ca === 'int') ? 'International' : 'Canadian');
  $title .= ', ' . (($career === 'ugrad') ? 'Undergrad' : 'Grad');
  drupal_set_title($title);

  // Get appropriate SQL query.
  if ($career === 'ugrad') {
    $q = uw_fees_display_table_ugrad_query($term_id, $int_ca);
  }
  else {
    $q = uw_fees_display_table_grad_query($term_id, $int_ca);
  }
  // Process query results into array.
  $a = array('term_id' => $term_id);
  $query = uw_fees_db()->query($q, $a);
  $program = array();
  while ($year_data = $query->fetch()) {
    $year_data = (array) $year_data;

    // Remove from $year_data fields that will be used as the keys in the output array.
    $pkey = array();
    foreach (array('group_name', 'program_title', 'program_term') as $field) {
      $pkey[$field] = $year_data[$field];
      unset($year_data[$field]);
    }

    // Convert all numbers to actual integers, preserving NULLs.
    $year_data = array_map(function ($value) {
      if ($value !== NULL) {
        $value = intval($value);
      }
      return $value;
    }, $year_data);

    $program[$pkey['group_name']][$pkey['program_title']][$pkey['program_term']] = $year_data;
  }

  $output = array();
  $output['tuition'] = theme('uw_fees_fee_table', array(
    'full_program' => $program,
    'show_tuition_5th_course' => ($int_ca === 'int') ? FALSE : TRUE,
    'career' => $career,
  ));
  $output['incidental'] = uw_fees_display_incidental_by_term($term_id);
  return $output;
}

/**
 *
 */
function uw_fees_display_table_ugrad_query($term_id, $int_ca) {
  $q = 'SELECT
    group_name,
    program_title,
    -- Combine into one string all year numbers to which these fee amounts apply to.
    GROUP_CONCAT(program_term ORDER BY program_term SEPARATOR \', \') AS program_term,
    tuition_max,
    tuition_per_course,
    coop,
    wrmf,
    fee_total_amount,

    tuition_max - (tuition_per_course * 4) AS tuition_5th_course,
    tuition_max + COALESCE(coop, 0) + COALESCE(wrmf, 0) + fee_total_amount AS tuition_fee_total

    FROM (SELECT
      COALESCE(group_full_name, group_short_name) AS group_name,
      program_title,
      tuition_set_term.program_term,

      (SELECT SUM(amount_ft) FROM fee_amount
        JOIN fee_set_fee USING(fee_id)
        WHERE (
          fee_set_fee.fee_set_id = COALESCE(program_program_term.fee_set_id, program.fee_set_id)
  ';
  // Add UHIP for internationals, different amounts for co-op or reg.
  if ($int_ca === 'int') {
    $q .= '      OR fee_id = CASE
              LENGTH(tuition_set_term.program_term)
                WHEN 2 THEN \'UHIP_coop\'
                ELSE \'UHIP_reg\'
              END
    ';
  }
  $q .= '      )
          -- Some fees not changed during some terms. "term_charged = 0" means the fee is charged all terms.
          AND ((tuition_set_term.term_id % 10) = term_charged OR term_charged = 0)
          -- Get version of fee currently in effect. @todo Verify that this actually works, deploy to other versions of this query.
          AND effective_term_id = (SELECT MAX(effective_term_id) FROM fee_amount WHERE fee_amount.fee_id = fee_set_fee.fee_id AND effective_term_id <= tuition_set_term.term_id)
      ) AS fee_total_amount,
  ';
  if ($int_ca === 'int') {
    $q .= '
      -- Similar to the COALESCE() for tuition_max, but using the NULL/NOT NULL values of for tuition_max to choose which version of tuition_per_course to use. This means if there is no tuition_per_course permitted, the value can be NULL and it will not COALESCE through to the faculty default.
      -- CASE instead of: COALESCE(int_tuition_set.int_tuition_per_course, tuition_set_term.int_tuition_per_course, tuition_set_faculty_default.int_tuition_per_course) AS tuition_per_course,
      CASE WHEN int_tuition_set.int_tuition_max IS NOT NULL
        THEN int_tuition_set.int_tuition_per_course
      WHEN tuition_set_term.int_tuition_max IS NOT NULL
        THEN tuition_set_term.int_tuition_per_course
      ELSE
        tuition_set_faculty_default.int_tuition_per_course
      END AS tuition_per_course,
      COALESCE(int_tuition_set.int_tuition_max, tuition_set_term.int_tuition_max, tuition_set_faculty_default.int_tuition_max) AS tuition_max,
    ';
  }
  else {
    $q .= '
      tuition_set_term.ca_tuition_per_course AS tuition_per_course,
      tuition_set_term.ca_tuition_max AS tuition_max,
    ';
  }
  $q .= '
      tuition_set_term.coop,
      tuition_set_term.wrmf

    FROM program
      JOIN uw_group USING (group_code)
      LEFT JOIN tuition_set_term USING (tuition_set_id)
      LEFT JOIN program_program_term USING (program_title, program_term)
      LEFT JOIN tuition_set_term int_tuition_set ON (
        COALESCE(int_tuition_set_id, program.tuition_set_id) = int_tuition_set.tuition_set_id
        AND tuition_set_term.term_id = int_tuition_set.term_id
        AND int_tuition_set.program_term = \'00\'
      )
      -- This sets the faculty default tuitition as the tuition for "Regular". The name of the default must match b.program_title.
      LEFT JOIN tuition_set_term tuition_set_faculty_default ON (
        tuition_set_faculty_default.tuition_set_id = (SELECT COALESCE(int_tuition_set_id, tuition_set_id) FROM program b WHERE b.group_code = program.group_code AND b.program_title LIKE \'Regular%\')
        AND tuition_set_term.term_id = tuition_set_faculty_default.term_id
        AND tuition_set_faculty_default.program_term = \'00\'
      )

    WHERE tuition_set_term.term_id = :term_id
      AND tuition_set_term.program_term != \'00\'
      AND group_code != \'GRAD\'
  ) sub

  GROUP BY -- Everything in SELECT list except program_term, which has GROUP_CONCAT().
    group_name,
    program_title,
    tuition_max,
    tuition_per_course,
    coop,
    wrmf,
    fee_total_amount,
    tuition_fee_total,
    tuition_5th_course

  ORDER BY
    -- Faculty
    group_name,
    -- Put Regular and Co-op last, rest alphabetically.
    program_title LIKE \'Co-op%\', program_title LIKE \'Regular%\',
    program_title,
    program_term';
  return $q;
}

/**
 *
 */
function uw_fees_display_table_grad_query($term_id, $int_ca) {
  $q = 'SELECT
    \'\' AS group_name,
    program_title,
    -- Combine into one string all year numbers to which these fee amounts apply to.
    GROUP_CONCAT(program_term ORDER BY program_term SEPARATOR \', \') AS program_term,

    tuition_max,
    fee_total_amount,
    tuition_max + fee_total_amount AS tuition_fee_total,

    pt_tuition,
    pt_per_course,
    pt_fees,
    pt_tuition + pt_fees AS pt_total

    FROM (SELECT
      program_title,
      tuition_set_term.program_term,
      tuition_set_term.pt_per_course,

      (SELECT SUM(fee_amount) FROM fee
        LEFT JOIN fee_set_fee USING(fee_id)
        WHERE fee_set_fee.fee_set_id = COALESCE(program_program_term.fee_set_id, program.fee_set_id)
          -- Some fees not changed during spring term. Set term_not_charged = 5.
          AND ((tuition_set_term.term_id % 10) != fee.term_not_charged OR fee.term_not_charged IS NULL)
      )
  ';

  if ($int_ca === 'int') {
    $uhip_grad = '  + (
        -- Add UHIP_grad only for internationals
        SELECT fee_amount FROM fee WHERE fee_id = \'UHIP_grad\'
      )
    ';
    $q .= $uhip_grad;
  }
  $q .= ' AS fee_total_amount,

      (SELECT SUM(fee_amount) FROM fee
        LEFT JOIN fee_set_fee USING(fee_id)
        WHERE fee_set_fee.fee_set_id = program.pt_fee_set_id
          -- Some fees not changed during spring term. Set term_not_charged = 5.
          AND ((tuition_set_term.term_id % 10) != fee.term_not_charged OR fee.term_not_charged IS NULL)
      )
  ';
  if ($int_ca === 'int') {
    $q .= $uhip_grad;
  }
  $q .= ' AS pt_fees,
  ';

  if ($int_ca === 'int') {
    $q .= '
      COALESCE(tuition_set_term.int_tuition_max, tuition_set_term.ca_tuition_max) AS tuition_max,
      COALESCE(tuition_set_term.pt_int_tuition, tuition_set_term.pt_ca_tuition) AS pt_tuition
    ';
  }
  else {
    $q .= '
      tuition_set_term.ca_tuition_max AS tuition_max,
      tuition_set_term.pt_ca_tuition AS pt_tuition
    ';
  }
  $q .= '

    FROM program
      JOIN uw_group USING (group_code)
      LEFT JOIN tuition_set_term USING (tuition_set_id)
      LEFT JOIN program_program_term USING (program_title, program_term)
      LEFT JOIN tuition_set_term int_tuition_set ON (
        COALESCE(int_tuition_set_id, program.tuition_set_id) = int_tuition_set.tuition_set_id
        AND tuition_set_term.term_id = int_tuition_set.term_id
        AND int_tuition_set.program_term = \'00\'
      )

    WHERE tuition_set_term.term_id = :term_id
      AND tuition_set_term.program_term != \'00\'
      AND group_code = \'GRAD\'
  ) sub

  WHERE tuition_max IS NOT NULL OR pt_tuition IS NOT NULL

  GROUP BY
    program_title,
    tuition_max,
    fee_total_amount,
    tuition_fee_total,
    pt_tuition

  ORDER BY
    -- Put Regular and Co-op first, rest alphabetically.
    fee_total_amount IS NULL,
    program_title,
    program_term';
  return $q;
}

/**
 *
 */
function theme_uw_fees_fee_table(array $variables) {
  if ($variables['career'] === 'ugrad') {
    $summary = t('One row per year of the program with columns for each fee or group of fees. Total fees charged in last column.');
    $header = array(
      'program' => t('Program'),
      'year' => t('Year/Term'),
      'tuition_per_course' => t('Price per 0.5 unit course'),
      'tuition_5th_course' => t('Fee for the fifth course'),
      'tuition_max' => t('Maximum tuition per term'),
      'coop' => t('Co-op fee'),
      'wrmf' => t('Work report marking fee'),
      'fee_total_amount' => t('Incidental fees'),
      'tuition_fee_total' => t('Total fees'),
    );
    if ($variables['show_tuition_5th_course']) {
      $header['tuition_per_course'] .= ' for the first four courses';
    }
    else {
      unset($header['tuition_5th_course']);
    }
  }
  else {
    $summary = t('One row per program with columns for various fees.');
    $header = array(
      'program' => array('colspan' => 2, 'data' => t('Program')),
      // 'year' => t('Year/Term'),.
      'tuition_max' => t('Full-time tuition per term'),
      'fee_total_amount' => t('Full-time incidental fees'),
      'tuition_fee_total' => t('Full-time total'),
      'pt_tuition' => t('Part-time tuition'),
      'pt_fees' => t('Part-time incidental fees'),
      'pt_total' => t('Part-time total'),
    );
  }

  $rows = array();
  foreach ($variables['full_program'] as $faculty => $programs) {
    if ($faculty) {
      $rows[] = array(array('colspan' => count($header), 'header' => TRUE, 'data' => $faculty));
    }
    foreach ($programs as $stream => $years) {
      $first = TRUE;
      foreach ($years as $year => $fee_data) {
        $row = array();
        foreach (array_keys($header) as $cell) {
          if ($cell === 'program') {
            // Calculate whether the year column should be show.
            if (isset($header['year'])) {
              // Single-year programs do not display a year column.
              $show_year = (bool) $year;
              $year_prefix = '';
            }
            else {
              // Only show if fees vary year to year.
              $show_year = count($years) > 1;
              $year_prefix = 'terms ';
            }

            if ($first) {
              $this_cell = array('rowspan' => count($years), 'data' => htmlspecialchars($stream));
              if (!$show_year) {
                $this_cell['colspan'] = 2;
              }
              $row[] = $this_cell;
              $first = FALSE;
            }
            if ($show_year) {
              $row[] = $year_prefix . $year;
            }
          }
          elseif ($cell === 'year') {
            // Handled above.
          }
          elseif ($fee_data[$cell]) {
            $this_cell = uw_fees_format_money($fee_data[$cell]);
            if ($cell === 'pt_tuition') {
              if ($fee_data['pt_per_course']) {
                $this_cell .= ' per course';
              }
              else {
                $this_cell .= ' per term';
              }
            }
            $row[] = $this_cell;
          }
          else {
            $row[] = '';
          }
        }
        $rows[] = array('data' => $row, 'no_striping' => TRUE);
      }
    }
  }

  return array(
    '#theme' => 'table',
    '#attributes' => array(
      'summary' => $summary,
    ),
    '#header' => $header,
    '#rows' => $rows,
  );
}

/**
 *
 */
function uw_fees_display_incidental_by_term($term_id) {
  $output = array();
  $output['header']['#markup'] = '<h2>Incidental Fees</h2>';

  // Query all fee types.
  $q = 'SELECT DISTINCT fee_type_id, fee_type_name
    FROM {fee} JOIN {fee_type} USING(fee_type_id)
    ORDER BY fee_type_name';
  $fee_types = uw_fees_db()->query($q);

  // Prepare query of all fees in a fee type.
  $q = 'SELECT *
    FROM {fee}
    JOIN {fee_amount} USING(fee_id)
    WHERE fee_type_id = :fee_type_id
      AND effective_term_id = (SELECT MAX(effective_term_id) FROM fee_amount fee_amount_INNER WHERE fee_amount_INNER.fee_id = fee.fee_id AND effective_term_id <= :term_id)
      AND (term_charged = 0 OR term_charged = :this_term)
    ORDER BY fee_title';
  $prep = uw_fees_db()->prepareQuery($q);
  $a = array('term_id' => $term_id, 'this_term' => $term_id % 10);

  // Foreach fee type, make a table of the fees in that fee type.
  while ($fee_type = $fee_types->fetch()) {
    $a['fee_type_id'] = $fee_type->fee_type_id;
    $prep->execute($a);

    $rows = array();
    while ($fee = $prep->fetch()) {
      $rows[] = array(
        'data' => array(
          $fee->fee_title,
          uw_fees_format_money($fee->amount_ft),
          uw_fees_format_money($fee->amount_pt),
        ),
        'no_striping' => TRUE,
      );
    }
    $output[$fee_type->fee_type_id]['header']['#markup'] = '<h3>' . $fee_type->fee_type_name . '</h3>';
    $output[$fee_type->fee_type_id]['content'] = array(
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => array('Fee title', 'Full-time', 'Part-time'),
    );
  }

  return $output;
}
