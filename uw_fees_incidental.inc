<?php

/**
 * @file
 * Functions relating to displaying information about incidental fees.
 */

/**
 *
 */
function uw_fees_display_incidental() {
  $output = array();

  $output['amounts_header']['#markup'] = '<h2>Fee amounts</h2>';
  $output['amounts'] = uw_fees_display_incidental_amounts();

  $output['sets_header']['#markup'] = '<h2>Fee sets</h2><p>Which fees are contained in each fee set.</p>';
  $output['sets'] = uw_fees_display_incidental_sets();

  return $output;
}

/**
 *
 */
function uw_fees_display_incidental_amounts() {
  $output = array();

  $q = 'SELECT * FROM {fee} ORDER BY fee_title';
  $r = uw_fees_db()->query($q)->fetchAll();

  $header = array('Effective term', 'Fall', 'Winter', 'Spring', 'All terms', 'Part-time all terms');

  $q = 'SELECT * FROM {fee_amount} WHERE fee_id = :fee_id ORDER BY effective_term_id, term_charged';
  $prep = uw_fees_db()->prepareQuery($q);
  foreach ($r as $fee) {
    $prep->execute(array('fee_id' => $fee->fee_id));
    $fees_processed = array();
    while ($fee_amount = $prep->fetch()) {
      $fees_processed[$fee_amount->effective_term_id][$fee_amount->term_charged] = array('amount_ft' => $fee_amount->amount_ft, 'amount_pt' => $fee_amount->amount_pt);
    }

    $rows = array();
    foreach ($fees_processed as $term_id => $fee_amount) {
      $row = array(
        uw_value_lists_uw_term_number_to_string($term_id),
        isset($fee_amount[9]['amount_ft']) ? uw_fees_format_money($fee_amount[9]['amount_ft']) : NULL,
        isset($fee_amount[1]['amount_ft']) ? uw_fees_format_money($fee_amount[1]['amount_ft']) : NULL,
        isset($fee_amount[5]['amount_ft']) ? uw_fees_format_money($fee_amount[5]['amount_ft']) : NULL,
        isset($fee_amount[0]['amount_ft']) ? uw_fees_format_money($fee_amount[0]['amount_ft']) : NULL,
        isset($fee_amount[0]['amount_pt']) ? uw_fees_format_money($fee_amount[0]['amount_pt']) : NULL,
      );
      $rows[] = array('data' => $row, 'no_striping' => TRUE);
    }

    $output[$fee->fee_id]['header']['#markup'] = '<h3>' . $fee->fee_title . ' (' . $fee->fee_id . ')</h3>';
    if ($rows) {
      $output[$fee->fee_id]['content'] = array(
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => $rows,
      );
    }
    else {
      $output[$fee->fee_id]['content']['#markup'] = '<p>No fee amounts in database.</p>';
    }
  }
  return $output;
}

/**
 *
 */
function uw_fees_display_incidental_sets() {
  $output = array();

  $q = 'SELECT * FROM {fee_set} ORDER BY fee_set_id';
  $r = uw_fees_db()->query($q)->fetchAll();

  $q = 'SELECT * FROM {fee_set_fee} WHERE fee_set_id = :fee_set_id ORDER BY fee_id';
  $prep = uw_fees_db()->prepareQuery($q);
  foreach ($r as $fee_set) {
    $prep->execute(array('fee_set_id' => $fee_set->fee_set_id));

    $items = array();
    while ($fee_id = $prep->fetch()) {
      $items[] = $fee_id->fee_id;
    }

    $output[$fee_set->fee_set_id]['header']['#markup'] = '<h3>' . $fee_set->fee_set_id . '</h3>';

    if ($items) {
      $output[$fee_set->fee_set_id]['content'] = array(
        '#theme' => 'item_list',
        '#items' => $items,
        '#type' => 'ul',
      );
    }
    else {
      $output[$fee->fee_id]['content']['#markup'] = '<p>No fee amounts in database.</p>';
    }
  }
  return $output;
}
